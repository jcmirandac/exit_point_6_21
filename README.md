# Makercase

Go to:

www.makercase.com

and select the basic box:

![](makercase_001.png)

Set all the options with the information from the images (scroll down on makercase to see more options):

![](makercase_002.png)

Then, use the download button, disable panel labels and download the svg file:

![](makercase_003.png)

Go to your downloads folder and right-click the file, then select **open with Inkscape**.

![](box_001.png)

You should see something like the image below.

![](box_002.png)

Use the selection tool to select the image an go to **Object** > **ungroup**. You should see each panel is now separated.

![](box_003.png)

![](box_004.png)

Then, go to google and look for images of natural foods that can be used as dyes. Copy the image and paste it in Inkscape. Remember to use **Trace bitmap** tool to convert all images.

![](box_005.png)

Change the size of the image and place it in the center of the first panel.

![](box_006.png)

Add images to the rest of the panels. And in one of the panels, write your name. Then use the selection tool to select the text, and go to **Path** > **object to path**.

![](box_007.png)

When you finish, save the file as **svg** and turn it in.
